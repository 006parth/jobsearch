import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchjobComponent } from './searchjob/searchjob.component';
import { JoblistComponent } from './joblist/joblist.component';

const routes: Routes = [
  { path: '', component: SearchjobComponent },
  { path: 'joblist', component: JoblistComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
