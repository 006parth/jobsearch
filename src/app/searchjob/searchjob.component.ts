import { Component, OnInit } from '@angular/core';
import { JobsService } from '../services/jobs.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { isFulfilled } from 'q';

@Component({
  selector: 'app-searchjob',
  templateUrl: './searchjob.component.html',
  styleUrls: ['./searchjob.component.scss']
})
export class SearchjobComponent implements OnInit {
  keyword: any;
  location: any = '';
  ip: any = ""
  jobs: any;
  total: any;
  start: any;
  loading: boolean = false;
  error: boolean = false;
  open: boolean = false;
  dataModel: any;
  config = {
    displayKey: "name",
    search: true,
    height: '300px',
    placeholder: 'Select',
    // customComparator: ()=>{} // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
    // limitTo: options.length // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
    // moreText: 'more' // text to be displayed whenmore than one items are selected like Option 1 + 5 more
    // noResultsFound: 'No results found!' // text to be displayed when no items are found while searching
    // searchPlaceholder:'Search',
    // searchOnKey: 'name'
  }
  constructor(public jobService: JobsService, private _snackBar: MatSnackBar,
    public dataService: DataService, public router: Router) {
    this.dataModel = this.dataService.key;
  }

  ngOnInit() {
    // this.getIpAddress();
    localStorage.clear();
  }
  menuClick() {
    if (this.open) {
      this.open = false;
    } else {
      this.open = true;
    }
  }

  getIpAddress() {
    this.loading = true;
    this.jobService.getIp().subscribe((res) => {
      this.ip = res.ip;
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    })
  }
  getKeywords(event) {
    // console.log(event);    
  }

  openDropDown() {
    let ref = document.getElementById('completer');
    ref.click();
  }

  changeKeyword(event) {
    if (event && event.value) {
      this.keyword = event.value[0];
    }

    // this.keyword = event.value[0].name;
  }

  findJobs() {
    console.log(this.keyword);

    if (this.keyword && this.location) {
      this.error = false;
      localStorage.setItem('loc', this.location);
      localStorage.setItem('key', this.keyword);
      // this.dataService.keyword = this.keyword;
      // this.dataService.location = this.location;
      this.router.navigate(['joblist']);
    }
    else if (this.location) {
      this.error = false;
      localStorage.setItem('loc', this.location);
      // this.dataService.location = this.location;
      this.router.navigate(['joblist']);
    }
    else {
      this.error = true;
    }

    // if (this.location.charAt(0) === '"' && this.location.charAt(this.location.length - 1) === '"') {
    //   console.log(this.location.substr(1, this.location.length - 2));
    // }
    // this.loading = true;
    // let data = {
    //   key: this.keyword,
    //   loc: this.location,
    //   ip: this.ip
    // }
    // this.jobService.findJobs(data).subscribe((res) => {
    //   this.total = res.total;
    //   this.jobs = res.jobs;
    //   this.loading = false;
    // }, err => {
    //   console.log(err.message);
    //   this.loading = false;
    //   this.openSnackBar(err.message);
    // })
  }

  loadMore() {
    this.loading = true;
    let data = {
      key: this.keyword,
      loc: this.location,
      ip: this.ip,
      start: this.start
    }
    this.jobService.findJobs(data).subscribe((res) => {
      this.total = res.total;
      this.jobs = res.jobs;
      this.loading = false;
    }, err => {
      console.log(err.message);
      this.loading = false;
      this.openSnackBar(err.message);
    })
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }

}
