import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { JobsService } from '../services/jobs.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-joblist',
  templateUrl: './joblist.component.html',
  styleUrls: ['./joblist.component.scss']
})
export class JoblistComponent implements OnInit {
  keyword: any;
  location: any;
  ip: any = "";
  total: any;
  jobsList: any;
  totalPages: any;
  loading: boolean = false;
  pagination: any;
  start: any = 0;
  filter: any = 'r';
  dataModel: any;

  constructor(public route: ActivatedRoute,
    private _snackBar: MatSnackBar, public dataService: DataService, public jobService: JobsService) {
    this.dataModel = this.dataService.key;
    //   if (this.dataService.keyword || this.dataService.location) {
    //   this.keyword = this.dataService.keyword;
    //   this.location = this.dataService.location;
    //   localStorage.setItem('loc',this.location);
    //   localStorage.setItem('key',this.keyword);
    //   this.getIpAddress();
    // } else {
    //   this.getIpAddress();
    // }
    if (localStorage.getItem('loc')) {
      if (localStorage.getItem('loc') && localStorage.getItem('key')) {
        this.keyword = localStorage.getItem('key');
        this.location = localStorage.getItem('loc');
        this.getIpAddress();
      } else {
        this.location = localStorage.getItem('loc');
        this.getIpAddress();
      }
    }
  }


  ngOnInit() {
  }

  getIpAddress() {
    this.loading = true;
    this.jobService.getIp().subscribe((res) => {
      this.ip = res.ip;
      if (this.keyword || this.location) {
        this.findJobs();
      }
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    })
  }

  setFilter(type) {
    if (this.filter === type) {

    } else {
      this.filter = type;
      this.findJobs();
    }
  }

  findJobs() {
    // if (this.location.charAt(0) === '"' && this.location.charAt(this.location.length - 1) === '"') {
    //   console.log(this.location.substr(1, this.location.length - 2));
    // }
    if (this.keyword || this.location) {
      localStorage.setItem('loc',this.location);
      localStorage.setItem('key',this.keyword);
      this.loading = true;
      let data = {
        key: this.keyword,
        loc: this.location,
        ip: this.ip,
        sort: this.filter
      }
      this.jobService.findJobs(data).subscribe((res) => {
        this.total = res.total;
        this.jobsList = res.jobs;
        this.totalPages = Math.ceil(this.total / 10);
        this.loading = false;
        this.pagination = this.getPager(this.total, 1, 10);
      }, err => {
        console.log(err.message);
        this.loading = false;
        this.openSnackBar(err.message);
      })
    }
  }
  loadMore() {
    this.loading = true;
    this.jobsList = [];
    let data = {
      key: this.keyword,
      loc: this.location,
      ip: this.ip,
      sort: this.filter,
      start: this.start
    }
    this.jobService.loadMore(data).subscribe((res) => {
      this.total = res.total;
      this.jobsList = res.jobs;
      this.loading = false;
    }, err => {
      this.loading = false;
      this.openSnackBar(err.message);
    })
  }
  openSnackBar(message: string) {
    this._snackBar.open(message, 'Close', {
      duration: 2000,
    });
  }

  apply(job) {
    console.log(job);
    console.log(job.url);
    window.open(job.url);
  }
  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    // ensure current page isn't out of range
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }
  setPage(page: number) {
    this.pagination = this.getPager(this.total, page);
    let startValue = page - 1
    if (startValue) {
      this.start = startValue * 10;
      this.loadMore();
    } else {
      this.start = 0;
      this.loadMore();
    }
  }
}
