import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchjobComponent } from './searchjob/searchjob.component';
import { JobsService } from './services/jobs.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import { JoblistComponent } from './joblist/joblist.component';
import {MatDividerModule} from '@angular/material/divider';
import { Ng2CompleterModule } from "ng2-completer";
import { SelectDropDownModule } from 'ngx-select-dropdown'
@NgModule({
  declarations: [
    AppComponent,
    SearchjobComponent,
    JoblistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule,
    SelectDropDownModule,
    MatCardModule,
    MatSnackBarModule,
    MatListModule,
    MatDividerModule,
    Ng2CompleterModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
